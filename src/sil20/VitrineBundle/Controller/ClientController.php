<?php

namespace sil20\VitrineBundle\Controller;

use sil20\VitrineBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Client controller.
 *
 */
class ClientController extends Controller
{

    /**
     * Lists all client entities.
     *
     */
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $userManager = $this->container->get('fos_user.user_manager');
            $users = $userManager->findUsers();

            return $this->render('sil20VitrineBundle:Client:index.html.twig', array(
                'title' => 'Liste des utilisateurs',
                'clients' => $users,
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    /**
     * Creates a new client entity.
     *
     */

    public function newAction(Request $request)
    {
        $client = new Client();
        $form = $this->createForm('sil20\VitrineBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->container->get('security.password_encoder');
            // On récupère l'encodeur défini dans security.yml
            $encoded = $encoder->encodePassword($client, $client->getPassword());
            // On encode le mot de passe issu du formulaire
            $client->setPassword($encoded);
            $em->persist($client);
            $em->flush();
            $session = $request->getSession();
            // store an attribute for reuse during a later user request
            $session->set('client', $client->getId());
            $this->addFlash(
                'notice', array(
                    'alert' => 'success',
                    'title' => 'Succès!',
                    'message' => 'Utilisateur ajouté!'
                )
            );
            return $this->redirectToRoute('client_show', array(
                'id' => $client->getId()
            ));
        }
        return $this->render('sil20VitrineBundle:Client:new.html.twig', array(
            'title' => 'Ajouter utilisateur',
            'client' => $client,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a client entity.
     *
     */
    public function showAction($id)
    {
        $client = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Client')->find($id);

        return $this->render('sil20VitrineBundle:Client:show.html.twig', array(
            'client' => $client,
            'title' => 'Client'
        ));
    }

    /**
     * Displays a form to edit an existing client entity.
     *
     */
    public function editAction(Request $request, Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('sil20\VitrineBundle\Form\ClientType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'notice', array(
                    'alert' => 'success',
                    'title' => 'Succès!',
                    'message' => 'Utilisateur mis à jour!'
                )
            );
            return $this->redirectToRoute('client_edit', array('id' => $client->getId()));
        }

        return $this->render('sil20VitrineBundle:Client:edit.html.twig', array(
            'client' => $client,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'title' => 'Modifier informations'
        ));
    }

    /**
     * Creates a form to delete a client entity.
     *
     * @param Client $client The client entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_delete', array('id' => $client->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a client entity.
     *
     */
    public function deleteAction(Request $request, Client $client)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->deleteUser($client);

        return $this->redirectToRoute('client_index');
    }
}
