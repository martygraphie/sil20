<?php

namespace sil20\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Article controller.
 *
 */
class AdminController extends Controller
{
    public function dashboardAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository('sil20VitrineBundle:Client')->findAll();
        $qte_article = $em->getRepository('sil20VitrineBundle:Article')->getNbArticleSale();
        $turnover = $em->getRepository('sil20VitrineBundle:Article')->getTurnOver();
        $commandes = $em->getRepository('sil20VitrineBundle:Commande')->getLastCommande();
        return $this->render('sil20VitrineBundle:Admin:dashboard_BO.html.twig', array(
            'title' => 'Dashboard',
            'clients' => $clients,
            'qte_article' => $qte_article,
            'turnover' => $turnover,
            'commandes' => $commandes
        ));
    }
}
