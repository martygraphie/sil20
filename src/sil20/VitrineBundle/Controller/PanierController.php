<?php

namespace sil20\VitrineBundle\Controller;

use sil20\VitrineBundle\Entity\Article;
use sil20\VitrineBundle\Entity\Commande;
use sil20\VitrineBundle\Entity\LigneCommande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use sil20\VitrineBundle\Entity\Panier;


class PanierController extends Controller
{
    public function contenuPanierAction()
    {
        $session = $this->container->get('request')->getSession();
        $panier = $session->has('panier') ? new Panier($session->get('panier')) : new Panier();
        $articles = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Article')->findAll();
        $quantite = array_sum($panier->getContenu());
        $montant = 0;
        if (!empty($panier->getContenu())) {
            foreach ($panier->getContenu() as $id_produit => $qte) {
                $montant = $montant + $articles[$id_produit - 1]->getPrix() * $qte;
            }
        }
        return $this->render('sil20VitrineBundle:Panier:contenuPanier.html.twig', array(
            'quantite' => $quantite,
            'montant' => $montant
        ));
    }

    public function panierAction()
    {
        $session = $this->container->get('request')->getSession();
        $panier = $session->has('panier') ? new Panier($session->get('panier')) : new Panier();
        $session->set('panier', $panier->getContenu());
        $articles = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Article')->findAll();
        $montant = 0;
        if (!empty($panier->getContenu())) {
            foreach ($panier->getContenu() as $id_produit => $quantite) {
                $montant = $montant + $articles[$id_produit - 1]->getPrix() * $quantite;
            }
        }
        return $this->render('sil20VitrineBundle:Panier:panier.html.twig', array(
            'panier' => $session->get('panier'),
            'articles' => $articles,
            'montant' => $montant
        ));
    }

    public function ajoutArticleAction($id, $qte = 1)
    {
        $session = $this->container->get('request')->getSession();
        $panier = $session->has('panier') ? new Panier($session->get('panier')) : new Panier();
        $panier->ajoutArticle($id, $qte);
        $session->set('panier', $panier->getContenu());

        return $this->redirect($this->generateUrl('panier'));

    }

    public function diminuerArticleAction($id, $qte = 1)
    {
        $session = $this->container->get('request')->getSession();
        $panier = new Panier($session->get('panier'));
        $panier->diminuerArticle($id, $qte);
        $session->set('panier', $panier->getContenu());

        return $this->redirect($this->generateUrl('panier'));

    }

    public function supprimerArticleAction($id)
    {
        $session = $this->container->get('request')->getSession();
        $panier = new Panier($session->get('panier'));
        $panier->supprimerArticle($id);
        $session->set('panier', $panier->getContenu());

        return $this->redirect($this->generateUrl('panier'));
    }

    public function validationAction()
    {
        $session = $this->container->get('request')->getSession();
        if ($session->has('panier')) {
            $em = $this->getDoctrine()->getManager();
            $panier = $session->get('panier');
            $client = $em->getRepository('sil20VitrineBundle:Client')->find($this->getUser()->getId());

            $commande = new Commande();
            $commande->setClient($client)->setDate(new \DateTime());
            $em->persist($commande);
            $em->flush();
            foreach ($panier as $idArticle => $quantite) {
                $article = $em->getRepository('sil20VitrineBundle:Article');
                $article = $article->find($idArticle);
                if ($quantite < $article->getStock()) {
                    $ligne = new LigneCommande();
                    $ligne->setCommande($commande)->setArticle($article)->setQuantite($quantite)->setPrix($quantite * $article->getPrix());
                    $commande->addLigneCommande($ligne);
                    $em->persist($ligne);
                    $em->flush();
                } else {

                    $qte = $article->getStock() > 0 ? $article->getStock() : ' aucun ';
                    $message = 'Vous avez commandé ' . $quantite . ' ' . $article->getLibelle() . '. Actuellement Indesmarket en a ' . $qte . ' en stock';

                    $this->addFlash(
                        'notice', array(
                            'alert' => 'warning',
                            'title' => 'Attention!',
                            'message' => $message
                        )
                    );
                    return $this->redirectToRoute('panier');
                }
            }
            $em->persist($commande);
            $em->flush();
            $this->viderPanierAction();
            return $this->render('sil20VitrineBundle:Panier:validationPanier.html.twig', array('commande' => $commande));
        } else {
            return $this->redirectToRoute('panier');
        }
    }

    public function viderPanierAction()
    {
        $session = $this->container->get('request')->getSession();
        $panier = new Panier($session->get('panier'));
        $panier->viderPanier();
        $session->set('panier', $panier->getContenu());

        return $this->redirect($this->generateUrl('panier'));
    }
}