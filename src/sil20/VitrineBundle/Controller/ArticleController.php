<?php

namespace sil20\VitrineBundle\Controller;

use Proxies\__CG__\sil20\VitrineBundle\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use sil20\VitrineBundle\Entity\Article;
use sil20\VitrineBundle\Form\ArticleType;

/**
 * Article controller.
 *
 */
class ArticleController extends Controller
{
    public function plusVendusAction()
    {
        // rechercher en BD les "$max" articles les plus vendus

        $em = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Article');
        $topsale = $em->getTopSale();

        return $this->render('sil20VitrineBundle:Article:plusVendus.html.twig', array(
            'topsale' => $topsale
        ));
    }

    public function articleParCategorieAction($id)
    {
        $em = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Categorie');
        # Récuperation des catégories
        $categorie = $em->find($id);
        $categories = $em->findAll();
        
        if (!$categorie) {
            throw $this->createNotFoundException('Catégories non trouvées');
        }
        return $this->render('sil20VitrineBundle:Article:article_categorie.html.twig', array(
            'categorie' => $categorie,
            'categories' => $categories
        ));
    }
    /**
     * Lists all article entities.
     *
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Categorie')->findAll();
        return $this->render('sil20VitrineBundle:Article:index.html.twig', array(
            'categories' => $categories,
            'title' => 'Liste des articles'
        ));
    }

    /**
     * Creates a new article entity.
     *
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('sil20\VitrineBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* Convertion de l'image en string*/
            $file = $article->getImage();
            $fileName = $this->get('app.image_uploader')->upload($file);
            $article->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush($article);
            $this->addFlash(
                'notice', array(
                    'alert' => 'success',
                    'title' => 'Succès!',
                    'message' => 'Article ajouté!'
                )
            );

            return $this->redirectToRoute('article_show', array(
                'id' => $article->getId(),
                'backOffice' => true
            ));
        }

        return $this->render('sil20VitrineBundle:Article:new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
            'title' => 'Ajouter article'
        ));
    }

    /**
     * Finds and displays a article entity.
     * @param : $backOficce
     * Permet de tester si on se trouve dans le Back office ou non, le rendu et les elements affichés sont differents dans le BO
     */
    public function showAction(Article $article)
    {
        return $this->render('sil20VitrineBundle:Article:show.html.twig', array(
            'article' => $article,
            'back_office' => false,
            'title' => 'Article'
        ));


    }

    public function showAdminAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('sil20VitrineBundle:Article:show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
            'back_office' => true,
            'title' => 'Article'
        ));


    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     */
    public function editAction(Request $request, Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('sil20\VitrineBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            /* Convertion de l'image en string*/
            $file = $article->getImage();
            $fileName = $this->get('app.image_uploader')->upload($file);
            $article->setImage($fileName);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'notice', array(
                    'alert' => 'success',
                    'title' => 'Succès!',
                    'message' => 'Article mis à jour!'
                )
            );
            return $this->redirectToRoute('article_edit', array('id' => $article->getId()));
        }

        return $this->render('sil20VitrineBundle:Article:edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'title' => 'Modifier l\'article'
        ));
    }

    /**
     * Deletes a article entity.
     *
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush($article);
        }

        return $this->redirectToRoute('article_index');
    }
}
