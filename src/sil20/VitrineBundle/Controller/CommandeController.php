<?php

namespace sil20\VitrineBundle\Controller;

use sil20\VitrineBundle\Entity\Commande;
use sil20\VitrineBundle\Entity\LigneCommande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Commande controller.
 *
 */
class CommandeController extends Controller
{

    /**
     * Lists all commande entities.
     *
     */
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Commande');
        $commandes = $securityContext->isGranted('ROLE_ADMIN') ? $em->findAll() : $em->findBy(array('client' => $this->getUser()->getId()));

        return $this->render('sil20VitrineBundle:Commande:index.html.twig', array(
            'commandes' => $commandes,
            'title' => 'Commandes'
        ));
    }

    /**
     * Creates a new commande entity.
     *
     */
    public function newAction(Request $request)
    {
        $commande = new Commande();
        $form = $this->createForm('sil20\VitrineBundle\Form\CommandeType', $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush($commande);

            return $this->redirectToRoute('commande_show', array('id' => $commande->getId()));
        }

        return $this->render('sil20VitrineBundle:Commande:new.html.twig', array(
            'title' => 'Ajouter une commande',
            'commande' => $commande,
            'form' => $form->createView(),

        ));
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function validationAction(Commande $commande)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($commande->getLigneCommande() as $lCommande) {
            $article = $em->getRepository('sil20VitrineBundle:Article')->find($lCommande->getArticle()->getId());
            $article->setStock($lCommande->getArticle()->getStock() - $lCommande->getQuantite());
            $em->persist($article);
            $em->flush($article);
        }

        return $this->redirectToRoute('sil20_vitrine_homepage');
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function showAction(Commande $commande)
    {
        $deleteForm = $this->createDeleteForm($commande);

        return $this->render('sil20VitrineBundle:Commande:show.html.twig', array(
            'title' => 'Recapitulatif commande',
            'commande' => $commande,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a commande entity.
     *
     * @param Commande $commande The commande entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Commande $commande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commande_delete', array('id' => $commande->getId(), 'idClient' => $commande->getClient())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing commande entity.
     *
     */
    public function editAction(Request $request, Commande $commande)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('sil20\VitrineBundle\Form\CommandeType', $commande);
        $editForm = $editForm->remove('client')->remove('date');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'notice', array(
                    'alert' => 'success',
                    'title' => 'Succès!',
                    'message' => 'Commande mise à jour!'
                )
            );
            return $this->redirectToRoute('commande_index', array(
                'title' => 'Commandes',
            ));
        } else {

        }

        return $this->render('sil20VitrineBundle:Commande:edit.html.twig', array(
            'title' => 'Modifier commande',
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Deletes a commande entity.
     *
     */
    public function deleteAction(Request $request, Commande $commande)
    {
        $form = $this->createDeleteForm($commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commande);
            $em->flush($commande);
        }

        return $this->redirectToRoute('commande_index');
    }
}
