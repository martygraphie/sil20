<?php

namespace sil20\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('sil20VitrineBundle:Default:index.html.twig');
    }

    public function mentionsAction()
    {
        return $this->render('sil20VitrineBundle:Default:mentions.html.twig');
    }

    public function contactAction()
    {
        return $this->render('sil20VitrineBundle:Default:contact.html.twig');
    }

    public function catalogueAction()
    {
        # Récuperation des catégories
        $categories = $this->getDoctrine()->getManager()->getRepository('sil20VitrineBundle:Categorie')->findAll();

        if (!$categories) {
            throw $this->createNotFoundException('Catégories non trouvées');
        }
        return $this->render('sil20VitrineBundle:Catalogue:catalog_default.html.twig', array(
            'categories' => $categories
        ));
    }
}