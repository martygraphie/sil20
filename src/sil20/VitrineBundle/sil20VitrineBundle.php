<?php

namespace sil20\VitrineBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class sil20VitrineBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
