<?php

namespace sil20\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Client
 */
class Client extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $nom;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $commandes;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->commandes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add commandes
     *
     * @param \sil20\VitrineBundle\Entity\Commande $commandes
     * @return Client
     */
    public function addCommande(\sil20\VitrineBundle\Entity\Commande $commandes)
    {
        $this->commandes[] = $commandes;

        return $this;
    }

    /**
     * Remove commandes
     *
     * @param \sil20\VitrineBundle\Entity\Commande $commandes
     */
    public function removeCommande(\sil20\VitrineBundle\Entity\Commande $commandes)
    {
        $this->commandes->removeElement($commandes);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * Remove commandes
     *
     * @param \sil20\VitrineBundle\Entity\Commande $commandes
     */
    public function removeAllCommandes()
    {
        foreach ($this->commandes as $commande)
            $this->commandes->removeElement($commande);
    }
}
