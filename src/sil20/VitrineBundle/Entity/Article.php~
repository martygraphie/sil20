<?php

namespace sil20\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use sil20\VitrineBundle\FileUploader;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $prix;

    /**
     * @var integer
     */
    private $stock;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ligneCommande;

    /**
     * @var \sil20\VitrineBundle\Entity\Categorie
     */
    private $categorie;
    
    private $image;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ligneCommande = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function __toString() {
        return $this->getLibelle();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Article
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set prix
     *
     * @param string $prix
     * @return Article
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Article
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Add ligneCommande
     *
     * @param \sil20\VitrineBundle\Entity\LigneCommande $ligneCommande
     * @return Article
     */
    public function addLigneCommande(\sil20\VitrineBundle\Entity\LigneCommande $ligneCommande)
    {
        $this->ligneCommande[] = $ligneCommande;

        return $this;
    }

    /**
     * Remove ligneCommande
     *
     * @param \sil20\VitrineBundle\Entity\LigneCommande $ligneCommande
     */
    public function removeLigneCommande(\sil20\VitrineBundle\Entity\LigneCommande $ligneCommande)
    {
        $this->ligneCommande->removeElement($ligneCommande);
    }

    /**
     * Get ligneCommande
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLigneCommande()
    {
        return $this->ligneCommande;
    }

    /**
     * Set categorie
     *
     * @param \sil20\VitrineBundle\Entity\Categorie $categorie
     * @return Article
     */
    public function setCategorie(\sil20\VitrineBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \sil20\VitrineBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }
    
    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }
}
