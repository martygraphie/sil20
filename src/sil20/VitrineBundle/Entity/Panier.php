<?php

namespace sil20\VitrineBundle\Entity;
class Panier
{
    private $contenu;

    # Constructeur panier
    public function __construct($contenu = array()) {
        $this->contenu = $contenu;
    }

    public function getContenu() {
        return $this->contenu;
    }

    public function setContenu($panier) {
        return $this->contenu = $panier;
    }
    # Ajoute un article au panier
    public function ajoutArticle ($id, $qte = 1) {
        if(!empty($this->contenu)){
            $this->contenu[$id] = array_key_exists ( $id , $this->contenu ) ?  $this->contenu[$id] + $qte : $qte ;
        }
        else{
            $this->contenu[$id] = $qte ;
        }
    }

    # diminue la quantité d'un article du panier
    public function diminuerArticle($id , $qte = 1) {
        $this->contenu[$id] = $this->contenu[$id] - $qte;
        if($this->contenu[$id] == 0 ){
            unset($this->contenu[$id]);
        }
    }
    # Supprime un article du panier
    public function supprimerArticle($id) {
        unset($this->contenu[$id]);
    }

    #Vide l'integralité du panier
    public function viderPanier() {
        $this->contenu = array();
    }
    
}