<?php

namespace sil20\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 */
class Commande
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var boolean
     */
    private $etat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ligneCommande;

    /**
     * @var \sil20\VitrineBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ligneCommande = new \Doctrine\Common\Collections\ArrayCollection();
        $this->etat = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Commande
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Add ligneCommande
     *
     * @param \sil20\VitrineBundle\Entity\LigneCommande $ligneCommande
     * @return Commande
     */
    public function addLigneCommande(\sil20\VitrineBundle\Entity\LigneCommande $ligneCommande)
    {
        $this->ligneCommande[] = $ligneCommande;

        return $this;
    }

    /**
     * Remove ligneCommande
     *
     * @param \sil20\VitrineBundle\Entity\LigneCommande $ligneCommande
     */
    public function removeLigneCommande(\sil20\VitrineBundle\Entity\LigneCommande $ligneCommande)
    {
        $this->ligneCommande->removeElement($ligneCommande);
    }

    /**
     * Get ligneCommande
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLigneCommande()
    {
        return $this->ligneCommande;
    }

    /**
     * Set client
     *
     * @param \sil20\VitrineBundle\Entity\Client $client
     * @return Commande
     */
    public function setClient(\sil20\VitrineBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \sil20\VitrineBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
}
